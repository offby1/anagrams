#!/usr/bin/env python3.0

import collections
import os
import re
from typing import DefaultDict, Iterable, Set

from .bag import Bag

has_a_vowel_re = re.compile(r"[aeiouy]")
long_enough_re = re.compile(r"^i$|^a$|^..")
non_letter_re = re.compile(r"[^a-zA-Z]")


def bogus_two_letter_word(w: str) -> bool:
    if len(w) != 2:
        return False

    # It's easier to spell out the legit words than the bogus ones
    return w not in {
        "am",
        "an",
        "as",
        "at",
        "ax",
        "be",
        "by",
        "do",
        "ha",
        "he",
        "hi",
        "ho",
        "id",
        "if",
        "in",
        "is",
        "it",
        "me",
        "my",
        "no",
        "of",
        "ok",
        "on",
        "or",
        "so",
        "to",
        "us",
    }


def word_acceptable(w: str) -> bool:
    if non_letter_re.search(w):
        return False
    if not long_enough_re.match(w):
        return False
    if not has_a_vowel_re.search(w):
        return False
    if bogus_two_letter_word(w):
        return False

    return True


default_dict_name = os.path.join(os.path.dirname(__file__), "words.utf8")


def snarf_dictionary_from_IO(seq: Iterable[str]) -> DefaultDict[Bag, Set[str]]:
    words_by_bag = collections.defaultdict(set)
    for w in seq:
        w = w.rstrip()
        w = w.lower()
        if not word_acceptable(w):
            continue

        words_by_bag[Bag(w)].add(w)

    return words_by_bag


def snarf_dictionary(fn: str) -> DefaultDict[Bag, Set[str]]:
    with open(fn, encoding="utf_8") as fh:
        return snarf_dictionary_from_IO(fh)
