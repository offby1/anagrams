#!/usr/bin/env python3
# see also https://github.com/wheerd/multiset#overview

from __future__ import annotations

from typing import Optional


class Bag(object):
    _primes = [
        2,
        3,
        5,
        7,
        11,
        13,
        17,
        19,
        23,
        29,
        31,
        37,
        41,
        43,
        47,
        53,
        59,
        61,
        67,
        71,
        73,
        79,
        83,
        89,
        97,
        101,
    ]

    __slots__ = "num"

    def __init__(self, str_: str) -> None:
        self.num = 1

        for c in str_.lower():
            if (c >= "a") and (c <= "z"):
                self.num *= self._primes[ord(c) - ord("a")]

    def empty(self) -> bool:
        return self.num == 1

    def __repr__(self) -> str:
        return repr(self.num)

    def __eq__(self, other: object) -> bool:
        return isinstance(other, Bag) and self.num == other.num

    def __hash__(self) -> int:
        return self.num.__hash__()

    def __sub__(self, other: object) -> Optional[Bag]:
        assert isinstance(other, Bag)
        quotient, remainder = divmod(self.num, other.num)
        if 0 == remainder:
            rv = Bag("")
            rv.num = quotient
            return rv
        else:
            return None
