#!/usr/bin/env python3

import argparse
import pathlib
import sys
import tempfile
from typing import Any, Iterable, List, Set, Tuple

import tqdm  # type: ignore

from . import dict
from .bag import Bag


def _p(*args: Any) -> None:
    print(*args, file=sys.stderr)


def combine(words: Set[str], anagrams: List[List[str]]) -> List[List[str]]:
    rv = []
    for w in words:
        for a in anagrams:
            rv.append([w] + a)

    return rv


def anagrams(
    b: Bag,
    dict_as_list: Iterable[Tuple[Bag, Set[str]]],
    progress_bar: tqdm.tqdm,
) -> List[List[str]]:
    rv = []

    pruned = [(key, words) for (key, words) in dict_as_list if (b - key) is not None]

    while pruned:
        (key, words) = pruned[0]
        smaller_bag = b - key

        assert smaller_bag
        if smaller_bag.empty():
            for w in words:
                rv.append([w])
                if progress_bar is not None:
                    progress_bar.update()
        else:
            from_smaller_bag = anagrams(smaller_bag, pruned, progress_bar=None)
            if from_smaller_bag:
                moar = combine(words, from_smaller_bag)
                rv.extend(moar)
                if progress_bar is not None:
                    progress_bar.update(len(moar))

        pruned = pruned[1:]

    return rv


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--dictionary",
        default=dict.default_dict_name,
        metavar="FILE",
        help="location of word list",
    )
    parser.add_argument("input", nargs="+")

    args = parser.parse_args()

    words_by_bag = dict.snarf_dictionary(args.dictionary)

    the_phrase = " ".join(args.input)
    the_input_bag = Bag(the_phrase)

    with tqdm.tqdm(
        unit="found",
    ) as progress_bar:
        result = anagrams(the_input_bag, words_by_bag.items(), progress_bar)

    _p(f"{len(result)} anagrams of {the_phrase!r}")

    # larger score is worse
    def _anagram_sort_score(words: List[str]) -> Tuple[float, float]:
        maxlen = max(map(len, words))
        minlen = min(map(len, words))
        evenness = abs(maxlen - minlen)
        return (len(words), evenness)

    with open(pathlib.Path(tempfile.gettempdir()) / the_phrase, "w") as outf:
        for a in sorted(result, key=_anagram_sort_score):
            print("({})".format(" ".join(a)), file=outf)

    _p(f"Results written to {outf.name!r}")


if __name__ == "__main__":
    main()
