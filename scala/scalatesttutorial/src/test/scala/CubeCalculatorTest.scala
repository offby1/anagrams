class WoofyPoofy extends org.scalatest.FunSuite {
  test("Snooty.Patootie") {
    assert(CubeCalculator.cube(3) === 27)
  }
  test("Huey.Lewis") {
    assert(CubeCalculator.cube(10) === 1000)
  }
}
