import scala.io.Source



object LineReader extends App {
  val bufferedSource = Source.fromFile("/etc/passwd")
  for (line <- bufferedSource.getLines) {
    println(line)
  }

  bufferedSource.close
}
