To run the tests from the command line (at least on my Mac, where I installed maven with "brew"):

`JAVA_HOME=$HOME/Library/Java/JavaVirtualMachines/openjdk-19.0.1/Contents/Home mvn test`
