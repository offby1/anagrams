package org.example;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        // TODO -- use the dictionary in this project (i.e., ../../../../../../words.utf8)
        // This will probably entail that I learn about resources &c
        var theDictionary = new AnagramsDictionary("/usr/share/dict/words");
        for (String arg : args) {
            System.out.printf("Hello arg: %s\n", arg);
            var multiset = new HashableCharacterCount(arg.toLowerCase());
            System.out.println(multiset);
        }
    }
}
