package org.example;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.nio.file.Files.readAllLines;

public class AnagramsDictionary {
    HashMap<String, Set<String>> wordListsByCounts;

    public AnagramsDictionary(String fileName) throws IOException {
        this(readAllLines(Path.of(fileName)));
    }

    public AnagramsDictionary(List<String> lines) {
        wordListsByCounts = new HashMap<>();
        for (var line : lines) {
            line = line.toLowerCase();
            if (isAcceptable(line)) {
                final var key = new HashableCharacterCount(line).toString();
                if (!wordListsByCounts.containsKey(key)) {
                    wordListsByCounts.put(key, new HashSet<>());
                }
                var previousValue = wordListsByCounts.get(key);
                previousValue.add(line);
                wordListsByCounts.put(key, previousValue);
            }
        }
    }

    private boolean isAcceptable(String word) {
        if (hasNonLetter(word)) {
            return false;
        }
        return !isBogusTwoLetterWord(word);
    }

    private boolean isBogusTwoLetterWord(String word) {
        if (word.length() != 2) {
            return false;
        }
        final var acceptableTwoLetterWords = Set.of("am", "an", "as", "at", "ax", "be", "by", "do", "ha", "he", "hi", "ho", "id", "if", "in", "is", "it", "me", "my", "no", "of", "ok", "on", "or", "so", "to", "us");
        return (!acceptableTwoLetterWords.contains(word));
    }

    private boolean hasNonLetter(String word) {
        for (Character ch : word.toCharArray()) {
            if (!Character.isLetter(ch)) return true;
        }
        return false;
    }
}
