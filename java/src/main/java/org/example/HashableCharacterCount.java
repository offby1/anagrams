package org.example;

import java.util.List;

public class HashableCharacterCount {
    final static List<Integer> primes = List.of(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101);

    Integer bigInteger;

    public HashableCharacterCount(final String arg) {
        bigInteger = 1;

        for (var ch : arg.toCharArray()) {

            if (Character.isLetter(ch)) {
                var primeIndex = Character.getNumericValue(Character.toLowerCase(ch))
                        - Character.getNumericValue('a');
                if (0 <= primeIndex && primeIndex < primes.size()) {
                    bigInteger *= primes.get(primeIndex);
                }
            }
        }
    }

    private HashableCharacterCount(Integer i) {
        bigInteger = i;
    }

    public String toString() {
        return this.bigInteger.toString();
    }

    public HashableCharacterCount minus(HashableCharacterCount bottom) {
        var remainder = this.bigInteger % bottom.bigInteger;
        if (remainder == 0) {
            var quotient = this.bigInteger / bottom.bigInteger;
            return new HashableCharacterCount(quotient);
        }
        return null;
    }

    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        var wat = (HashableCharacterCount) other;
        return this.bigInteger.equals(wat.bigInteger);
    }

    public int hashCode() {
        return this.bigInteger.hashCode();
    }
}
