package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HashableCharacterCountTest {
    @Test
    void expectedValues(){
        assertEquals("2", new HashableCharacterCount("a").toString());
        assertEquals("4", new HashableCharacterCount("aa").toString());
        assertEquals("3", new HashableCharacterCount("b").toString());
        assertEquals("5", new HashableCharacterCount("c").toString());
        assertEquals("30", new HashableCharacterCount("abc").toString());
        assertEquals("30", new HashableCharacterCount("cba").toString());
        assertEquals("1", new HashableCharacterCount("ć").toString());
    }

    @Test
    void subtraction() {
        final var top = new HashableCharacterCount("dog");
        final var bottom = new HashableCharacterCount("go");
        assertEquals(new HashableCharacterCount("d"), top.minus(bottom));
        assertEquals(null, top.minus(new HashableCharacterCount("xyzzy")));
    }
}
