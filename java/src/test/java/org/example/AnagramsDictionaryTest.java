package org.example;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AnagramsDictionaryTest {
    @Test
    void dogIsGod() {
        List<String> usrShareDictWords = List.of("dog", "god", "God");

        var words = new AnagramsDictionary(usrShareDictWords);
        System.out.println(words);
        assertEquals(1, words.wordListsByCounts.size());

        // no dups, neither
        var dogGod = words.wordListsByCounts.get((new HashableCharacterCount("dog")).toString());
        assertEquals(2, dogGod.size());
    }
}