#!/bin/sh

set -e
set -x

cd $(dirname $0)

export JAVA_HOME=$HOME/Library/Java/JavaVirtualMachines/openjdk-19.0.1/Contents/Home
PATH=$JAVA_HOME/bin:$PATH

mvn package

java -cp target/anagrams*.jar org.example.Main "Ernest Hemingway"
